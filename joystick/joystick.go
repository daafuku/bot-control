package joystick

import (
	"bufio"
	"encoding/binary"
	"fmt"
	logutil "log"
	"os"
)

var (
	log *logutil.Logger
)

func init() {
	log = logutil.New(os.Stderr, "[STIK] ", 0)
}

type Packet struct {
	time uint32
	key  uint16
	val  int16
}

func (p Packet) String() string {
	return fmt.Sprintf("[%08X] %04X %04X", p.time, p.key, uint16(p.val))
}

func NewPacket(time uint32, key uint16, val int16) Packet {
	return Packet{
		time: time,
		key:  key,
		val:  val,
	}
}

func (p Packet) Key() uint16 { return p.key }
func (p Packet) Val() int16  { return p.val }

type Joystick struct {
	fd  *os.File
	buf *bufio.Reader
	ouc chan Packet
}

func New(path string) (j *Joystick, err error) {
	j = &Joystick{}

	j.ouc = make(chan Packet)

	j.fd, err = os.Open(path)
	if err != nil {
		return nil, err
	}
	log.Println("joystick device opened")

	j.buf = bufio.NewReader(j.fd)
	log.Println("joystick buffer created")

	return
}

func (j *Joystick) Start() {
	log.Println("joystick driver started")
	go func() {
		for {
			var p Packet
			var err error

			err = binary.Read(j.buf, binary.LittleEndian, &p.time)
			if err != nil {
				break
			}

			err = binary.Read(j.buf, binary.LittleEndian, &p.val)
			if err != nil {
				break
			}

			err = binary.Read(j.buf, binary.LittleEndian, &p.key)
			if err != nil {
				break
			}

			j.ouc <- p
		}

		close(j.ouc)
		log.Println("joystick driver closed channel")

	}()
}

func (j *Joystick) Output() <-chan Packet {
	return j.ouc
}

func (j *Joystick) Close() {
	j.fd.Close()
}
