package joystick

import (
	"os"
	"testing"
)

func TestJoystick(t *testing.T) {
	j, err := New(os.Args[2])
	if err != nil {
		t.Fatal(err)
	}

	j.Start()

	var i int = 0

	for _ = range j.Output() {
		if i < 100 {
			i++
			continue
		}
		break
	}
}
