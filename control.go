package main

import (
	"./gui"
	"./joyparser"
	"./joystick"
	configutil "darfk/config"
	logutil "log"
	"net"
	"net/rpc"
	"os"
	"time"
	"os/exec"
)

type Config struct {
	RemoteNetworkHost   string
	LocalJoystickDevice string
	GUI                 gui.Config
}

var (
	config Config
	log    *logutil.Logger
)

func init() {
	configutil.MustLoad("config.json", &config)
	log = logutil.New(os.Stderr, "[CONT] ", 0)
}

func main() {

	var (
		err       error
		remote    *rpc.Client
		guiServer *gui.GUI
	)

	guiServer, err = gui.New(config.GUI)
	if err != nil {
		log.Fatalf("could not start gui server: %s\n", err)
	}

	guiServer.Start()

	{
		var conn net.Conn
		conn, err = net.DialTimeout("tcp4", config.RemoteNetworkHost, time.Second*3)
		if err != nil {
			log.Fatalf("could not connect to remote host: %s\n", err)
		}
		defer conn.Close()
		log.Println("established connection to bot")
		remote = rpc.NewClient(conn)
	}

	{
		var input string = "ping"
		var output string
		err = remote.Call("BotControl.Echo", input, &output)
		if err != nil {
			log.Fatalf("error returned from rpc: %s\n", err)
		}

		if output != input {
			log.Fatalf("bad response from echo %v != %v\n", output, input)
		}
		log.Println("echo received from bot")
	}

	joy, err := joystick.New(config.LocalJoystickDevice)
	if err != nil {
		log.Fatalf("could not create joystick driver: %s\n", err)
	}
	defer joy.Close()

	joy.Start()

	inc := joy.Output()
	pnc := make(chan joystick.Packet)

	go func() {
		for in := range inc {
			pnc <- in
		}
	}()
	
	ouc := make(chan joystick.Packet)

	var captureLock chan int = make(chan int, 1)

	captureLock <- 1
	
	{
		var fakeXButtonTicker *time.Ticker = time.NewTicker(time.Second * 15)
		go func() {
			for {
				<-fakeXButtonTicker.C
				pnc <- joystick.NewPacket(0, 0x0001, 0x0001)
			}
		}()
	}

	go func() {
		for in := range pnc {
			if in.Val() == 0x0001 && in.Key() == 0x0001 {
				select {
				case <-captureLock:
					go func() {
						defer func() {
							captureLock <- 1
						}()

						var imageData []byte

						if err := remote.Call("BotControl.CaptureImage", 1, &imageData); err != nil {
							log.Printf("could not capture image: %s\n", err)
							return
						}

						path := "capture-" + time.Now().Format(time.RFC3339) + ".jpg"

						fd, err := os.Create("images/" + path)
						if err != nil {
							log.Printf("could not create jpg file: %s\n", err)
							return
						}
						defer fd.Close()

						n, err := fd.Write(imageData)
						if err != nil {
							log.Printf("could not write image data: %s\n", err)
						} else {
							log.Printf("wrote %d bytes to %s\n", n, path)
							guiServer.UpdateCapturedPath(path)
						}

						{
							var darknet *exec.Cmd
							var err error
							darknet = exec.Command("./darknet", "detector", "test", "bilal_new.data", "bilal_new.cfg", "bilal_new.weights", "/home/tom/university/management/project/code/control/images/" + path)
							darknet.Dir = "/home/tom/university/management/project/code/darknet/build/darknet/x64"
							err = darknet.Run()
							if err != nil {
								log.Printf("error doing a darknet: %s\n", err)
								return
							}

							guiServer.UpdateClassifiedPath("/home/tom/university/management/project/code/darknet/build/darknet/x64/predictions.png")

							log.Println("darknet done")
						}

					}()
				default:
					log.Printf("tried to capture while capture operation already taking place\n")
				}
			}
			ouc <- in
		}
	}()

	joyparser, err := joyparser.New(ouc)
	if err != nil {
		log.Fatal("could not create joystick parser", err)
	}

	joyparser.Start()

	for {
		remote.Call("BotControl.SendSerialData", <-joyparser.Output(), nil)
	}
}
