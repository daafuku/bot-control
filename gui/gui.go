package gui

import (
	_ "fmt"
	logutil "log"
	"net/http"
	"os"
	"text/template"
)

var (
	log *logutil.Logger
)

func init() {
	log = logutil.New(os.Stderr, "[GUI ] ", 0)
}

type Config struct {
	BindAddress  string
	TemplatePath string
}

type GUI struct {
	server         *http.Server
	dirty          bool
	tmpl           *template.Template
	capturedPath   string
	classifiedPath string
}

func (g *GUI) UpdateCapturedPath(path string) {
	g.dirty = true
	g.capturedPath = path
}

func (g *GUI) UpdateClassifiedPath(path string) {
	g.dirty = true
	g.classifiedPath = path
}

func New(config Config) (g *GUI, err error) {
	g = &GUI{}

	g.tmpl, err = template.ParseFiles(config.TemplatePath + "/index.html")
	if err != nil {
		return nil, err
	}

	g.server = &http.Server{
		Addr: config.BindAddress,
	}

	http.HandleFunc("/capture.jpg", func(w http.ResponseWriter, r *http.Request) {
		log.Printf("[%s] %s", r.Method, r.URL.Path)
		if g.capturedPath == "" {
			http.Error(w, "no capture path set yet", http.StatusNotFound)
		}

		path := "images/" + g.capturedPath
		log.Printf("serving %s\n", path)

		header := w.Header()
		header["Cache-Control"] = []string{"no-cache"}
		http.ServeFile(w, r, path)
		return
	})

	http.HandleFunc("/classified.png", func(w http.ResponseWriter, r *http.Request) {
		log.Printf("[%s] %s", r.Method, r.URL.Path)
		if g.capturedPath == "" {
			http.Error(w, "no classified path set yet", http.StatusNotFound)
		}

		path := g.classifiedPath
		log.Printf("serving %s\n", path)

		header := w.Header()
		header["Cache-Control"] = []string{"no-cache"}
		http.ServeFile(w, r, path)
		return
	})

	// http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
	// 	log.Printf("[%s] %s", r.Method, r.URL.Path)

	// 	if r.URL.Path == "/poll" {
	// 		fmt.Fprintf(w, "%t", g.dirty)
	// 		return
	// 	}

	// 	http.Error(w, "Not Found", http.StatusNotFound)
	// })

	// http.HandleFunc("/dirty", func(w http.ResponseWriter, r *http.Request) {
	// 	fmt.Fprintf(w, "%t", g.dirty)
	// })

	// http.Handle("/images/", http.StripPrefix("/images", http.FileServer(http.Dir("images"))))

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if err := g.tmpl.ExecuteTemplate(w, "index.html", nil); err != nil {
			log.Println(err)
		}
	})

	return
}

func (g *GUI) Start() {
	go func() {
		log.Printf("starting GUI server")
		err := g.server.ListenAndServe()
		log.Printf("server closed: %s", err)
	}()
}
