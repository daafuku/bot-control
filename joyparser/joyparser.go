package joyparser

import (
	"../joystick"
	logutil "log"
	"os"
)

var (
	log *logutil.Logger
)

func init() {
	log = logutil.New(os.Stderr, "[PARS] ", 0)
}

type JoyParser struct {
	inc <-chan joystick.Packet
	ouc chan []byte
}

func New(inc <-chan joystick.Packet) (q *JoyParser, err error) {
	err = nil

	q = &JoyParser{
		inc: inc,
		ouc: make(chan []byte),
	}

	return
}

func (q *JoyParser) Start() {
	go func() {

		var send = func(a, b byte) {
			q.ouc <- []byte{0x02, 0x02, a, b, 0x02 ^ 0x02 ^ a ^ b}
		}

		for packet := range q.inc {

			buf := make([]byte, 4)

			buf[0] = byte(0x02)

			// Button Push
			if packet.Val() == 0x0001 && packet.Key()&0x0001 == 0x0001 {
				if packet.Key() == 0x0501 {
					send(byte('U'), byte('S'))
				} else if packet.Key() == 0x0601 {
					send(byte('C'), byte('M'))
				} else if packet.Key() == 0x0701 {
					send(byte('C'), byte('A'))
				}
			}

			// Analog
			if packet.Key() == 0x0402 || packet.Key() == 0x0102 {
				val := packet.Val()

				if abs(val) < 0x4000 {
					val = 0
				}

				val *= -1

				var side byte

				if packet.Key() == 0x0402 {
					side = byte('R')
				} else {
					side = byte('L')
				}

				send(side, byte((val>>8)&0xFF))
			}
		}

		close(q.ouc)
		log.Println("joystick parser closed channel")

	}()
}

func (j *JoyParser) Output() <-chan []byte {
	return j.ouc
}

func abs(in int16) int16 {
	if in < 0 {
		return in * -1
	}
	return in
}
